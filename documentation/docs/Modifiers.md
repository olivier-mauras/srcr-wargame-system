Modifiers table
===============

|     | Hit points | Attack | Challenge | Speed |
| --- | --- | --- | --- | --- |
| weapons |     | x   | x?  |     |
| Items (armor) | x   |     | x   | x   |
| Skills |     | x   |     | x   |
| Health state (Fatigue) |     | x   |     | x?  |
| Environment (Terrain) |     |     | x   | x   |
| Mount | x   | x   |     | x   |

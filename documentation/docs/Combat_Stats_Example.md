## Examples of combat stats

### Standard vs Standard
```
$ python3 calc/sscr_combat_v2.py -a 3 --aq 6 -d 3 --dq 6
Atk Q6+ - A/D3
  HIT:     33.81%
  WIN:     27.83%
  WIN 2x6: 6.33%
  DEF.:    5.88%
  DEF.2x6: 0.5% 
Def Q6+ - A/D3
  HIT:     33.68%
  WIN:     27.81%
  WIN 2x6: 6.34%
  DEF.:    5.98%
  DEF.2x6: 0.51%
###
TIE: 5.19%
```
### Standard vs Veteran
```
$ python3 calc/sscr_combat_v2.py -a 3 --aq 6 -d 4 --dq 5
Atk Q6+ - A/D3
  HIT:     31.89%
  WIN:     21.94%
  WIN 2x6: 5.86%
  DEF.:    6.77%
  DEF.2x6: 0.38% 
Def Q5+ - A/D4
  HIT:     48.81%
  WIN:     42.04%
  WIN 2x6: 10.34%
  DEF.:    9.95%
  DEF.2x6: 1.37%
###
TIE: 6.63%
```
### Veteran vs Elite
```
$ python3 calc/sscr_combat_v2.py -a 4 --aq 5 -d 5 --dq 4
Atk Q5+ - A/D4
  HIT:     37.38%
  WIN:     26.63%
  WIN 2x6: 9.58%
  DEF.:    7.72%
  DEF.2x6: 0.99% 
Def Q4+ - A/D5
  HIT:     47.04%
  WIN:     39.33%
  WIN 2x6: 14.42%
  DEF.:    10.75%
  DEF.2x6: 2.11%
###
TIE: 14.6%
```
### Elite vs Hero
```
$ python3 calc/sscr_combat_v2.py -a 5 --aq 4 -d 6 --dq 3
Atk Q4+ - A/D5
  HIT:     36.82%
  WIN:     26.65%
  WIN 2x6: 12.8%
  DEF.:    6.9%
  DEF.2x6: 1.74% 
Def Q3+ - A/D6
  HIT:     43.71%
  WIN:     36.8%
  WIN 2x6: 17.89%
  DEF.:    10.17%
  DEF.2x6: 3.05%
###
TIE: 19.47%
```

### Veteran vs Standard
```
$ python3 calc/sscr_combat_v2.py -a 4 --aq 5 -d 3 --dq 6
Atk Q5+ - A/D4
  HIT:     48.7%
  WIN:     41.52%
  WIN 2x6: 10.06%
  DEF.:    10.1%
  DEF.2x6: 1.39% 
Def Q6+ - A/D3
  HIT:     32.23%
  WIN:     22.13%
  WIN 2x6: 5.99%
  DEF.:    7.17%
  DEF.2x6: 0.41%
###
TIE: 6.73%
```

### 4x Standard vs Hero
```
$ python3 calc/srcr_combat.py -a 7 --aq 6 -d 6 --dq 3 --def-mod -2
Atk Q6+ - A/D7
  HIT:     46.04%
  WIN:     42.86%
  WIN 2x6: 23.17%
  DEF.:    20.9%
  DEF.2x6: 4.56% 
Def Q3+ - A/D6
  HIT:     40.1%
  WIN:     19.2%
  WIN 2x6: 8.48%
  DEF.:    3.18%
  DEF.2x6: 0.72%
###
TIE: 13.48%
```

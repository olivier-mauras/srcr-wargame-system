## Model Stats Table

| **Role<br>** | **Quality** | **Combat / Ability stat** | **Hit Points** | **Can group by (?)** |
| --- | --- | --- | --- | --- |
| **Hero** | Q4+ | 1 | 5 | 0   |
| **Elite** | Q5+ | 1 | 4 | 2 |
| **Veteran** | Q6+ | 0 | 3 | 3 |
| **Standard** | Q7+ | 0 | 2 | 4 |
| **Novice** | Q8+ | 0 | 1 | 5 |

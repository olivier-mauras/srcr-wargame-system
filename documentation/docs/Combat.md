## Combat resolution

The system is based on a **S**ingle **R**oll **C**ombat **R**esolution. Both player roll their dice at once and resolve the outcome simultaneously.  
It uses **3d6** as a comparison base, where the **lowest pair** is used as the model quality check, and the **highest pair** is the attack/defense result.  
As the resolution is simultaneous, depending on the roll, an attacker could end up losing the exchange...  

### Roll

- Both players roll x**d6** and keep **three**
    - **x** being their A/D stat **+**/**-** combat modifiers applied by the current condition - Terrain, Traits, etc
    - If **x > 3** always keep the **lowest die** + **the two highest**
    - This is called the **TDR** - ==Triple Dice Result==
- The **two lowest** of the **TDR** are used to make a **Quality check**
    - This is called the **LDP** - ==Low Dice Pair==
- The **two highest** of the **TDR** are the **Attack/Defense** result
    - This is called the **HDP** - ==High Dice Pair==

!!! note
    If **x < 3** roll can **only defend with two highest**  
    **Quality/Reaction checks** are auto failed

??? dice-roll "Example"
    A three dice roll looks like this:  
    :material-dice-2:{ .dice-medium } :material-dice-4:{ .dice-medium } :material-dice-6:{ .dice-medium} = **TDR**  
    :material-dice-2:{ .dice-medium } :material-dice-4:{ .dice-medium } = **LDP**  
    :material-dice-4:{ .dice-medium } :material-dice-6:{ .dice-medium} = **HDP**  

    If you roll more than **three** dice based on your [model A/D stats](././Model_Stats_Table.md#model-stats-table) or any combat modifiers like this roll:  
    :material-dice-1:{ .dice-medium } :material-dice-3:{ .dice-medium-orange } :material-dice-5:{ .dice-medium } :material-dice-6:{ .dice-medium}  

    Then your **TDR** will be:  
    :material-dice-1:{ .dice-medium } :material-dice-5:{ .dice-medium } :material-dice-6:{ .dice-medium}  
    :material-dice-1:{ .dice-medium } :material-dice-5:{ .dice-medium } = **LDP**  
    :material-dice-5:{ .dice-medium } :material-dice-6:{ .dice-medium} = **HDP**  


### Melee

Here below, the **Attacker** defines the model initiating the attack. With the reaction system, during the same exchange, the **Defender** can become the **Attacker** by initiating a reaction attack.  

- **Both** attacker and defender ==fail== their **quality checks**
    - They both miss and nothing happens
    - They can check for melee reaction - See [Reaction system](./index.md#reaction-system) _melee_ tab
- Any side **succeeds** their **quality checks**
    - If a side ==fails== their **quality checks**, they cannot inflict damage nor react, and can only **defend**
        - Although they can avoid certain death or being stunned with a higher **HDP** result, ==failing== their **quality checks** will certainly end up with an injury.
    - Compare both sides **HDP**
        - If one side's **HDP** is a double six, it's a critical hit and the other is directly removed from play - Except if the other is able to pull a tie!
        - If model **succeeded** their **quality checks** _and_ their **HDP** result is ==greater== than the other: The other is **Stunned**!
        - If their **HDP** result is ==less== than the other: The other successfully **Defended**
            - ==Successfully== **defending** without ==succeeding== the **quality checks** will allow other model to [roll for injury](./Combat.md#rolling-for-injury)!
        - If both **HDP** results are ==equals==: This is a **Tie** that can lead to a **Reaction** - See [Reaction system](./index.md#reaction-system) _melee_ tab - only the one(s) that suceeded their **quality checks** can react though!

!!! hint "Who can react?"
    There is two ways to react during a melee combat:  

    1. When both models failed their **quality checks** and _tied_ on miss
    2. When one or both models suceeded their **quality checks** and tied on **HDP** comparison.  In this case though **only** the one(s) succeeding the **quality checks** can react!

??? dice-roll "Combat Example - _Zolmar_ vs _Tigmoer_"
    _Zolmar_ the heinous is an Elite model. As per the [model stats table](./Model_Stats_Table.md) he has a quality of **4+** - meaning that he needs to get **4** or more on his **LDP** result to pass his **quality check** - and an **Attack/Defense** stat of **5** so he will roll **five** dice.  

    He decides to charge and attack _Tigmoer_ which is a Veteran model. As per the [model stats table](./Model_Stats_Table.md) _Tigmoer_ has a quality of **5+** and **Attack/Defense** stat of **4** so he will roll **four** dice.  

    _Zolmar_ rolls the following:  
    :material-dice-3:{ .dice-small-orange } :material-dice-1:{ .dice-small } :material-dice-5:{ .dice-small } :material-dice-6:{ .dice-small } :material-dice-4:{ .dice-small-orange }  
    :material-dice-1:{ .dice-small } :material-dice-5:{ .dice-small } = **LDP**  
    :material-dice-5:{ .dice-small } :material-dice-6:{ .dice-small } = **HDP**  
    _Zolmar_ successfully passes his **quality check** with a **LDP** result of **6**  

    _Tigmoer_ rolls at the same time:  
    :material-dice-1:{ .dice-small } :material-dice-6:{ .dice-small } :material-dice-4:{ .dice-small } :material-dice-1:{ .dice-small-orange }  
    :material-dice-1:{ .dice-small } :material-dice-4:{ .dice-small } = **LDP**  
    :material-dice-4:{ .dice-small } :material-dice-6:{ .dice-small } = **HDP**  
    _Tigmoer_ successfully passes his **quality check** as well with a **LDP** result of **5**  

    As they both passed their **quality check** they should now compare their **HDP** results.  
    _Zolmar_ **HDP** result is **11** while _Tigmoer_ **HDP** result is **10**. With only one point of difference, _Zolmar_ wins the combat and _Tigmoer_ is **stunned**.  

??? dice-roll "Combat Example - _Heirldrick_ vs _Guntar_"
    _Heirldrick_ is a Standard model. As per the [model stats table](./Model_Stats_Table.md) he has a quality of **6+** - he needs to get **6** or more on his **LDP** result to pass his **quality check** - and an **Attack/Defense** stat of **3** so he will roll **3** dice.  

    He is in base contact with _Guntar_ which is also a Standard model. They have the same stats and no combat modifiers to apply, so they just roll **three** dice each.

    _Heirldrick_ rolls the following:  
    :material-dice-4:{ .dice-small } :material-dice-2:{ .dice-small } :material-dice-4:{ .dice-small }  
    :material-dice-2:{ .dice-small } :material-dice-4:{ .dice-small } = **LDP**  
    :material-dice-4:{ .dice-small } :material-dice-4:{ .dice-small } = **HDP**  
    _Heirldrick_ successfully passes his **quality check** with a **LDP** result of **6**  

    _Guntar_ rolls at the same time:  
    :material-dice-2:{ .dice-small } :material-dice-6:{ .dice-small } :material-dice-1:{ .dice-small }  
    :material-dice-1:{ .dice-small } :material-dice-2:{ .dice-small } = **LDP**  
    :material-dice-2:{ .dice-small } :material-dice-6:{ .dice-small } = **HDP**  
    _Guntar_ fails his **quality check** with a **LDP** result of **3**, he won't be able to do any damage during this combat, but he can still be safe when comparing **HDP** results!  

    _Heirldrick_ **HDP** result is **8** and _Guntar_ got exactly the same result! It's a **tie**! _Guntar_'s defense was sufficient to save him in this exchange of blows... But as they end up with a **tie**, reactions are possible!  
    _Heirldrick_ which had successfully passed his **quality check** now has the opportunity to react - See [Reaction system](./index.md#reaction-system) _melee_ tab.  
    _Guntar_ sadly by failing his **quality check** cannot react and will have to defend himself if _Heirldrick_ succeeds his **reaction check**

### Shooting

#### Ranged weapons

Not all ranged weapons have the same performances, a short bow is less powerfull and shoots less far than a long bow, but it certainly is faster to reload and less exausting to use.

Weapon donut **min**/**max** with **negative** modifiers out of the ideal **distance**

- [ ] Still need to define
    - [ ] Exact distance per weapons
    - [ ] Range with negative modifiers

#### Resolution

!!! warning
    When shooting, defender doesn't issue damage and is only rolling for defense

- Attacker ==fails== their **quality check** it's a **miss**: Defender can check for missed fire reaction - See [Reaction system](./index.md#reaction-system) _On missed fire_ tab
- Attacker ==succeeds== their **quality check**, it could **hit**
    - Defender ==succeeds== their **quality check** and their **LDP** is ==greater or equal== than **Attacker**'s: Defender **Parried** and can react! See [Reaction system](./index.md#reaction-system) _On missed fire_ tab
    - Compare the **HDP** results
        - If **Attacker** wins, Defender model is **Stunned**
            - If **HDP** is a double six, it's a critical shot and **Defender** is directly removed from play - Except if defender is able to pull a tie!
        - If **Defender** wins, he can **React**. See [Reaction system](./index.md#reaction-system) _On missed fire_ tab
        - If both **HDP** results are ==equals==: This is a **Tie** that will lead to an injury for the Defender. Attacker can [roll for injury](./Combat.md#rolling-for-injury)

??? dice-roll "Shooting Example - _Yiseza_ vs _Sarov_"
    _Yiseza_ is a Veteran model. As per the [model stats table](./Model_Stats_Table.md) she has a quality of **5+** and an **Attack/Defense** stat of 4. She declares a shooting attack on _Sarov_, and she is located at the border of a forest, so she gets **+1d6** for **Ambush** bonus. She will thus roll **5** dice for her shooting attack.  

    _Sarov_ is a Standard model. He has a quality of **6+** and an **Attack/Defense** stat of **3**. He is standing unaware of being targeted from the forest nearby, but is luckily standing behind a parapet so gets **+1d6** as cover. He will thus roll **4** dice.  

    _Yiseza_ rolls the following:  
    :material-dice-6:{ .dice-small } :material-dice-1:{ .dice-small } :material-dice-3:{ .dice-small-orange } :material-dice-2:{ .dice-small-orange } :material-dice-6:{ .dice-small }  
    :material-dice-1:{ .dice-small } :material-dice-6:{ .dice-small } = **LDP**  
    :material-dice-6:{ .dice-small } :material-dice-6:{ .dice-small } = **HDP**  
    _Yiseza_ is succeeding her **quality check** as well as firing a critical hit! The only chance for _Sarov_ to survive is to roll a double six as well on his **HDP** result!  

    _Sarov_ rolls for his defense:  
    :material-dice-2:{ .dice-small } :material-dice-6:{ .dice-small } :material-dice-6:{ .dice-small } :material-dice-5:{ .dice-small-orange }  
    :material-dice-2:{ .dice-small } :material-dice-6:{ .dice-small } = **LDP**  
    :material-dice-6:{ .dice-small } :material-dice-6:{ .dice-small } = **HDP**  
    What were the odds?! _Sarov_ is in luck!! He exchanges a certain death against an injury!  

    As this is a Tie, _Yiseza_ rolls **1d6** to check what injury from the [Injury Table](./Combat.md#injury-table) _Sarov_ will receive.  
    :material-dice-1:{ .dice-small }  
    Luck is definitely on _Sarov_ side... He receives no injury, he is now aware of _Yiseza_ presence at the border of the woods and can react! See [Reaction system](./index.md#reaction-system) _On missed fire_ tab 
    
## Rolling for Injury

Model rolling for injury, rolls **1d6** to select an injury from the [Injury Table](./Combat.md#injury-table)

### Injury table

| **Roll<br>** | **Injury** |
| --- | --- |
| **1** | No wound happens => React check |
| **2** | Leg wound - Movement one stick down |
| **3** | Arm wound - Quality * **2** |
| **4** | Gut wound - Attack **-1d6** |
| **5** | Head wound - Q **\*2** - A/D Stat **-1d6** |
| **6** | **Stunned** |

## [Model Stats](./Model_Stats_Table.md)
## [Combat Stats Example](./Combat_Stats_Example.md)

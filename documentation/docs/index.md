Single Roll Over Challenge (Combat Resolution) System
=====================================================

## Introduction

Welcome to the Single Roll Over Challenge System documentation.  

The system has been designed primarily with skirmish style wargame in mind. The goal is to keep a lean and fast resolution using a single dice roll with as little bookeeping as possible.  
In case of combat, the resolution happens simultaneously from both sides.  

To keep to resolution interesting and consistent, a challenge based on **difficulty** is defined for the character to match. The **Challenge** is derived from the **quality** of the opposing character or **difficulty** of the action. 
It consist of a **low** and **high** value, **low** being the minimum value for a **success**, **high** being the maximum value for a **critical success**.  

The two main statistics used from the [Character's definition](./Model_Stats_Table.md) for the **challenge** resolution are **Quality** and **Combat/Ability** stats.

### Challenge definition

To define the challenge **high** and **low** numbers, this simple calculation is made:

XXX: To word properly

```
# 1 - Challenge over better quality - harder
if quality > opposite quality:
    low = quality
    high = quality + 2

# 2 - Challenge over lower quality - easier
if quality < opposite quality:
    low = quality -1
    high = quality +1

# 3 - Challenge over same quality
if quality == opposite quality:
    low = quality
    high = quality +1
```

Challenge modifiers are then applied if existing.

??? Example
    Hitting a character under cover is obviously more difficult and thus **cover** gives a **+1** challenge bonus to the defender.  
    Attacking character, thus needs to add **+1** to his **low** and **high** challenge values

    See [modifiers table](./Modifiers.md) for more details.

### Challenge resolution

Each character involved in the challenge resolution - more than *one* in case of combat - rolls **1d6** die at the same time.  
They then add their **combat/ability stat** and eventual modifiers to the roll and compare results to the challenge.  

XXX: To word properly

```
roll_result = 1d6 + (combat/ability stat + combat/ability modifier)
if roll_result < low:
    failed

if roll_result >= low:
    success

if roll_result >= high:
    success_critical
    roll_result = roll_result +1
```

When more than *one* character are involved in the challenge resolution, `roll_result` are compared. Higher **wins**. Draws can trigger a reaction.

## Actions

WIP  
[Roll](./Combat.md#roll) over challenge **difficulty** to succeed ?

## Reaction system

If not specifically stated, reaction requires **successfull quality check** to be executed

=== "**On sight**"
    Your ennemy model moves into line of sight. Any of your models that get LoS can try one of the following reactions:

    - **Shoot** - Requires a shooter model
    - **Charge**
        - Move into base contact with ennemy model and initiate [melee combat](./Combat.md#melee)
    - **Flee**
        - Model get movement to **+1** stick size
        - Flee twice => Coward trait => **+1Q**

=== "**On charge**"
    Your ennemy has declared a charge. You certainly won't wait for him like a sheep and can react with the following reactions:

    - **Shoot** - Requires a shooter model
        - Charging model defends at **-1d6** A/D
    - **Prepare for Impact**
        - Defending model get **+1d6** A/D bonus
    - **Counter charge**
        - Attacker gets charged in return and so:
            - Movement stopped
            - **-1d6** A/D
    - **Flee**
        - model get movement to **+1** stick size
        - Flee twice => Coward trait => **+1Q**
    - **Alert friendly target?**
        - ?

=== "**On missed fire**"
    Your model got shot at. Either shooter missed and your model is now alerted, or you sucessfully parried the shot.  
    Your model can now react with the following reactions:

    - **Return fire** - Requires a shooter model
    - **Take cover** - Find the closest protection possible
        - Model get free movement of **one short** stick
    - **Flee** - Get out of the combat zone as quickly as possible
        - Model get movement to **+1** stick size
        - Flee twice => Coward trait => **+1Q**

=== "**Melee**"
    During a [melee combat](./Combat.md#melee) there are two ways to react:

    - When both attacker and defender have missed their melee combat **quality check**, they both can react by doing a **reaction check**
        - Winner takes the initiative
        - Draw - reaction is cancelled and round continues
    - When **HDP** comparison leads to a tie. Same as above.

    Winner of the **reaction check** can chose the following reaction:

    - **Disengage** from melee by moving out of ennemy model by **one short** stick
    - **Attack** ennemy that is still in base contact

=== "**On death of a friend**"
    This reaction is **automatic** and **mandatory**. It doesn't require **reaction check**.  

    - **Morale check** - [Roll](./Combat.md#roll) 3 dice to succeed **Quality check** - when the following event happens:
        - Any friendly model killed in short stick distance
        - Any warband Hero killed no matter the distance

    If **morale check** ==is failed==, model has to flee in the opposite direction he is currently facing for **1 long** stick. He also gains the **Coward trait (+1Q)** until the end of turn.  
    If **morale check** ==succeeds==, then nothing happens.

## [Combat](./Combat.md#combat)


## Movement

SAGA sticks
